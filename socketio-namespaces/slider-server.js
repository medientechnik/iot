// Name des Socket.IO-Namespace, wird sowohl im Client als auch im Server verwendet
var NAMESPACE = '/slider';
module.exports = {
	// Definition der Funktion initialize(io); dies ist die einzige exportierte Funktion dieses Moduls
	initialize: function(io) {
		// Socket für den oben definierten Namespace öffnen
		var nsp = io.of(NAMESPACE);

		// weitere Initialisierung wie gewohnt...
		nsp.on('connection', function (socket) {
			socket.on('rangechanged',function(data) {
				socket.broadcast.emit('rangechanged', data);
				console.log(NAMESPACE + '/rangechanged: ');
				console.log(data);
			});
			socket.on('disconnect', function () {
				console.log("Someone disconnected");
			});
			console.log(NAMESPACE +  ": client connected " + socket);
		});
	}
}
