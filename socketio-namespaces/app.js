var PORT = 3000;
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
server.listen(PORT);

app.use(express.static(__dirname + '/public'));

var io = require('socket.io').listen(server);

// Server-Code für das Projekt "slider" einbinden und initialisieren
require('./slider-server.js').initialize(io);
// Server-Code für das Projekt "checkbox" einbindnen und initialisieren
require('./checkbox-server.js').initialize(io);
// weitere Module werden hier eingebunden...