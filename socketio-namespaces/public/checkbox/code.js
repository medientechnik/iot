function initialize(){
	// Definition des Namespace; wird sowohl im Client als auch im Server verwendet
	var NAMESPACE = '/checkbox';

	// Initialisierung des Client-Sockets für diesen Namespace
	var socket = io(NAMESPACE);
	
	// set event handler for checkbox click
	var checkbox = document.getElementById("checkbox");
	function clickHandler(){
		if (this.checked){
			socket.emit("checked", {"value": 1});
		}
		else{
			socket.emit("checked", {"value": 0});
		}
		console.log(this.checked);
	}
	checkbox.addEventListener("click", clickHandler);
	
	// set event handler that dispatches incoming  messages
	function onCheckboxChanged(data){
		if (data.value == 1){
			checkbox.checked = true;
		}
		else {
			checkbox.checked = false;
		}
	}
	socket.on("checkbox",onCheckboxChanged);
}