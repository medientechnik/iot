var PORT = 3000;

// Initialisierung des Express Servers
var express = require("express");
var app = express();

var http = require("http");
var server = http.createServer(app);
app.use(express.static(__dirname + "/public"));
server.listen(PORT);

// Initialisierung Websockets
var socketio = require("socket.io");
var io = socketio.listen(server);

// Eventhandler f�r die Verbindung eines neuen Clients
io.sockets.on("connection", function(socket){
	setInterval(function(){
		var random = Math.floor(Math.random() * 100);
		socket.emit("tick", {"value": random});
	}, 1000);
	
	socket.on("sliderChanged", function(data){
		console.log(data.value);
		socket.broadcast.emit("sliderChanged", data);
	});

});