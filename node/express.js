var PORT = 3000;
var express = require('express');
var app = express();

app.get("/", function(request, response){
	response.send("Welcome to Express");
});
app.get("/a", function(request, response){
	response.send("selected 'a'");
});
app.get("/add", function(request, response){
	var a = parseInt(request.query["a"]);
	var b = parseInt(request.query["b"]);
	var sum = a + b;
	response.send(a + " + " + b + " = " + sum);
});
app.listen(PORT);