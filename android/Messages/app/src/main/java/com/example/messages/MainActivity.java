package com.example.messages;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "com.example.message.MESSAGE";
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // EditText Objekt initialisieren
        editText = (EditText)findViewById(R.id.edit_message);

        // ListView initialisieren
        ListView listView = (ListView)findViewById(R.id.list_messages);
        final ArrayList<String> messageList = new ArrayList<String>();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, messageList);
        listView.setAdapter(arrayAdapter);


        Button button = (Button)findViewById(R.id.button_send);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // hide keyboard
                ((InputMethodManager)getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);

                String message = editText.getText().toString();
                editText.setText("");
                // Toast toast = Toast.makeText(MainActivity.this, "Message: " + message, Toast.LENGTH_LONG);
                // toast.show();

                messageList.add(message);
                arrayAdapter.notifyDataSetChanged();

                // send message to other activity
                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);

            }
        });
    }
}
