function initialize(){
	// localhost for testing with Android emulator
	// --> EDIT THIS:
	var HOST = "http://10.0.2.2:3000";
	
	var socket = io.connect(HOST);
	
	socket.on("tick", function(data){
		var value = data.value;
		document.getElementById("label").innerHTML = value;
	});
	
	var slider = document.getElementById("slider");
	slider.addEventListener("input", function(){
		var value = this.value;
		socket.emit("sliderChanged", {"value": value});
	});
	
	socket.on("sliderChanged", function(data){
		slider.value = data.value;
	});
}